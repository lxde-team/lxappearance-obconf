# Spanish translations for obconf package.
# Copyright (C) 2007 Dana Jansens
# This file is distributed under the same license as the obconf package.
# David Merino <rastiazul at yahoo . com>, 2007,
# Hugo Florentino <sysadmin@cips.cu>, 2010
msgid ""
msgstr ""
"Project-Id-Version: obconf 2.0.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-11-13 23:54+0100\n"
"PO-Revision-Date: 2014-09-17 02:31+0000\n"
"Last-Translator: Adolfo Jayme Barrientos <fitojb@ubuntu.com>\n"
"Language-Team: spanish\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.7\n"
"X-POOTLE-MTIME: 1410921069.000000\n"

#: ../src/main.c:177
msgid ""
"Failed to load the obconf.glade interface file. ObConf is probably not "
"installed correctly."
msgstr ""
"Falló la carga del archivo de interfaz obconf.glade. Probablemente OBConf no "
"está instalado correctamente."

#: ../src/main.c:206
msgid "Failed to load an rc.xml. Openbox is probably not installed correctly."
msgstr ""
"Falló la carga de un archivo rc.xml. Probablemente Openbox no está instalado "
"correctamente."

#: ../src/main.c:219
#, c-format
msgid ""
"Error while parsing the Openbox configuration file. Your configuration file "
"is not valid XML.\n"
"\n"
"Message: %s"
msgstr ""
"Ocurrió un error al analizar el archivo de configuración de Openbox. El "
"archivo no contiene sintaxis XML válida.\n"
"\n"
"Mensaje: %s"

#: ../src/theme.c:112 ../src/theme.c:144
msgid "Choose an Openbox theme"
msgstr "Seleccione un tema de Openbox"

#: ../src/theme.c:121
msgid "Openbox theme archives"
msgstr "Archivos de temas de Openbox"

#: ../src/archive.c:49
#, c-format
msgid "\"%s\" was installed to %s"
msgstr "«%s» se instaló en %s"

#: ../src/archive.c:74
#, c-format
msgid "\"%s\" was successfully created"
msgstr "Se ha creado «%s» correctamente"

#: ../src/archive.c:110
#, c-format
msgid ""
"Unable to create the theme archive \"%s\".\n"
"The following errors were reported:\n"
"%s"
msgstr ""
"No se pudo crear el archivo de tema «%s».\n"
"Se informó de los errores siguientes:\n"
"%s"

#: ../src/archive.c:115 ../src/archive.c:196
#, c-format
msgid "Unable to run the \"tar\" command: %s"
msgstr "No se pudo ejecutar la orden «tar»: %s"

#: ../src/archive.c:135
#, c-format
msgid "Unable to create directory \"%s\": %s"
msgstr "No se pudo crear la carpeta «%s»: %s"

#: ../src/archive.c:157
#, c-format
msgid "\"%s\" does not appear to be a valid Openbox theme directory"
msgstr "«%s» no parece ser una carpeta de temas de Openbox válida"

#: ../src/archive.c:167
#, c-format
msgid "Unable to move to directory \"%s\": %s"
msgstr "No se pudo mover al directorio «%s»: %s"

#: ../src/archive.c:203
#, c-format
msgid ""
"Unable to extract the file \"%s\".\n"
"Please ensure that \"%s\" is writable and that the file is a valid Openbox "
"theme archive.\n"
"The following errors were reported:\n"
"%s"
msgstr ""
"No se pudo extraer el archivo «%s».\n"
"Asegúrese de que «%s» es escribible y de que el archivo es un tema de "
"Openbox válido.\n"
"Se informó de los errores siguientes:\n"
"%s"

#: ../src/obconf.glade.h:1
msgid "<span weight=\"bold\">Theme</span>"
msgstr "<span weight=\"bold\">Tema</span>"

#: ../src/obconf.glade.h:2
msgid "_Install a new theme..."
msgstr "_Instalar un tema nuevo…"

#: ../src/obconf.glade.h:3
msgid "Create a theme _archive (.obt)..."
msgstr "Crear un _archivo de tema (.obt)…"

#: ../src/obconf.glade.h:4
msgid "Theme"
msgstr "Tema"

#: ../src/obconf.glade.h:5
msgid "Font for active window title:"
msgstr "Fuente para el título de la ventana activa:"

#: ../src/obconf.glade.h:6
msgid "Font for inactive window title:"
msgstr "Fuente para el título de la ventana inactiva:"

#: ../src/obconf.glade.h:7
msgid "_Button order:"
msgstr "_Orden de los botones:"

#: ../src/obconf.glade.h:8
msgid ""
"N: Window icon\n"
"L: Window label (Title)\n"
"I: Iconify (Minimize)\n"
"M: Maximize\n"
"C: Close\n"
"S: Shade (Roll up)\n"
"D: Omnipresent (On all desktops)"
msgstr ""
"N: Icono de ventana\n"
"L: Etiqueta de la ventana (título)\n"
"I: Iconificar (minimizar)\n"
"M: Maximizar\n"
"C: Cerrar\n"
"S: Sombra (replegar)\n"
"D: Omnipresente (en todos los escritorios)"

#: ../src/obconf.glade.h:15
msgid "Title Bar"
msgstr "Barra de título"

#: ../src/obconf.glade.h:16
msgid "Font for menu header:"
msgstr "Tipo de letra de la cabecera del menú:"

#: ../src/obconf.glade.h:17
msgid "Font for menu Item:"
msgstr "Tipo de letra del elemento del menú:"

#: ../src/obconf.glade.h:18
msgid "Font for on-screen display:"
msgstr "Tipo de letra de OSD:"

#: ../src/obconf.glade.h:19
msgid "Font for inactive on-screen display:"
msgstr "Tipo de letra de OSD inactivo:"

#: ../src/obconf.glade.h:20
msgid "Misc."
msgstr "Varias"

#~ msgid "Copyright (c)"
#~ msgstr "Copyright (c)"

#~ msgid "Syntax: obconf [options] [ARCHIVE.obt]\n"
#~ msgstr "Sintaxis: obconf [opciones] [ARCHIVO.obt]\n"

#~ msgid ""
#~ "\n"
#~ "Options:\n"
#~ msgstr ""
#~ "\n"
#~ "Opciones:\n"

#~ msgid "  --help                Display this help and exit\n"
#~ msgstr "  --help                Mostrar esta ayuda y salir\n"

#~ msgid "  --version             Display the version and exit\n"
#~ msgstr "  --version             Mostrar la versión y salir\n"

#~ msgid ""
#~ "  --install ARCHIVE.obt Install the given theme archive and select it\n"
#~ msgstr ""
#~ "  --install ARCHIVO.obt Instalar el archivo de tema dado y seleccionarlo\n"

#~ msgid ""
#~ "  --archive THEME       Create a theme archive from the given theme "
#~ "directory\n"
#~ msgstr ""
#~ "  --archive THEME       Crear un archivo de tema desde el directorio de "
#~ "tema dado\n"

#~ msgid ""
#~ "\n"
#~ "Please report bugs at %s\n"
#~ "\n"
#~ msgstr ""
#~ "\n"
#~ "Por favor reportar errores (bugs) a %s\n"
#~ "\n"

#~ msgid "Openbox Configuration Manager"
#~ msgstr "Administrador de la Configuración de Openbox"

#~ msgid "    "
#~ msgstr "    "

#~ msgid "<span weight=\"bold\">Windows</span>"
#~ msgstr "<span weight=\"bold\">Ventanas</span>"

#~ msgid "_Windows retain a border when undecorated"
#~ msgstr "_Las ventanas mantienen un borde cuando están sin decorar"

#~ msgid "A_nimate iconify and restore"
#~ msgstr "A_nimar iconificar y restaurar"

#~ msgid "<span weight=\"bold\">Window Titles</span>"
#~ msgstr "<span weight=\"bold\">Títulos de las Ventanas</span>"

#~ msgid ""
#~ "N - The window's icon\n"
#~ "D - The all-desktops (sticky) button\n"
#~ "S - The shade (roll up) button\n"
#~ "L - The label (window title)\n"
#~ "I - The iconify (minimize) button\n"
#~ "M - The maximize button\n"
#~ "C - The close button"
#~ msgstr ""
#~ "N - El icono de la ventana\n"
#~ "D - El botón de todos los (sticky) escritorios\n"
#~ "S - El botón de(arrollar hacia arriba) ocultar\n"
#~ "L - La etiqueta(titulo de la ventana)\n"
#~ "I - El botón de (minimizar) iconificar\n"
#~ "M - El botón de maximizar\n"
#~ "C - El botón de cerrar"

#~ msgid "<span weight=\"bold\">Fonts</span>"
#~ msgstr "<span weight=\"bold\">Fuentes</span>"

#~ msgid "_Active window title: "
#~ msgstr "_Titulo activo de la ventana: "

#~ msgid "_Menu Item: "
#~ msgstr "_Elemento del Menú: "

#~ msgid "Appearance"
#~ msgstr "Apariencia"

#~ msgid "<span weight=\"bold\">Focusing Windows</span>"
#~ msgstr "<span weight=\"bold\">Enfocando Ventanas</span>"

#~ msgid "Focus _new windows when they appear"
#~ msgstr "E_nfocar ventanas nuevas cuando éstas aparezcan"

#~ msgid "<span weight=\"bold\">Placing Windows</span>"
#~ msgstr "<span weight=\"bold\">Colocando Ventanas</span>"

#~ msgid "_Place new windows under the mouse pointer"
#~ msgstr "Colocar las ventanas nuevas debajo del _puntero del ratón"

#~ msgid "_Center new windows when they are placed"
#~ msgstr "_Centrar las ventanas nuevas cuando éstas son colocadas"

#~ msgid "Windows"
#~ msgstr "Ventanas"

#~ msgid "<span weight=\"bold\">Moving and Resizing Windows</span>"
#~ msgstr ""
#~ "<span weight=\"bold\">Moviendo y cambiando el tamaño de las ventanas</"
#~ "span>"

#~ msgid "Update the window contents while _resizing"
#~ msgstr ""
#~ "Actualiza_r los contenidos de las ventanas mientras cambia el tamaño"

#~ msgid "Drag _threshold distance:"
#~ msgstr "Arras_trar la distancia del umbral:"

#~ msgid "px"
#~ msgstr "px"

#~ msgid "Amount of resistance against other _windows:"
#~ msgstr "Cantidad de resistencia contra otras _ventanas:"

#~ msgid "Amount of resistance against screen _edges:"
#~ msgstr "Cantidad d_e resistencia contra los bordes de la pantalla:"

#~ msgid "ms"
#~ msgstr "ms"

#~ msgid "When resizing terminal windows"
#~ msgstr "Cuando cambia el tamaño de las ventanas de terminal"

#~ msgid "Always"
#~ msgstr "Siempre"

#~ msgid "Never"
#~ msgstr "Nunca"

#~ msgid "Centered on the window"
#~ msgstr "Centrado en la ventana"

#~ msgid "Above the window"
#~ msgstr "Sobre la ventana"

#~ msgid "_Focus windows when the mouse pointer moves over them"
#~ msgstr ""
#~ "En_focar las ventanas cuando el puntero del ratón se mueve sobre estas"

#~ msgid "Move focus under the mouse when _switching desktops"
#~ msgstr "Mover el enfoque bajo el ratón cuando _se cambian escritorios"

#~ msgid "Move focus _under the mouse when the mouse is not moving"
#~ msgstr ""
#~ "Mover el enfoq_ue debajo del ratón cuando el ratón no se está moviendo"

#~ msgid "_Raise windows when the mouse pointer moves over them"
#~ msgstr ""
#~ "Alza_r la ventanas cuando el puntero del ratón se mueve sobre éstas."

#~ msgid "_Delay before focusing and raising windows:"
#~ msgstr "Atrasar antes _de enfocar y elevar ventanas:"

#~ msgid "<span weight=\"bold\">Titlebar</span>"
#~ msgstr "<span weight=\"bold\">Barra de titulo</span>"

#~ msgid "Double click on the _titlebar:"
#~ msgstr "Doble click en la barra de _titulo:"

#~ msgid "Maximizes the window"
#~ msgstr "Maximiza la ventana"

#~ msgid "Shades the window"
#~ msgstr "Oculta la ventala"

#~ msgid "Double click ti_me:"
#~ msgstr "Dura_ción del doble click:"

#~ msgid "Mouse"
#~ msgstr "Ratón"

#~ msgid "<span weight=\"bold\">Desktops</span>"
#~ msgstr "<span weight=\"bold\">Escritorios</span>"

#~ msgid "_Number of desktops: "
#~ msgstr "_Cantidad de escritorios: "

#~ msgid "_Desktop names:"
#~ msgstr "_Nombre de los escritorios:"

#~ msgid "Desktops"
#~ msgstr "Escritorios"

#~ msgid "<span weight=\"bold\">Desktop Margins</span>"
#~ msgstr "<span weight=\"bold\">Márgenes de los Escritorios</span>"

#~ msgid ""
#~ "Desktop margins are reserved areas on the edge of your screen.  New "
#~ "windows will not be placed within a margin, and maximized windows will "
#~ "not cover them."
#~ msgstr ""
#~ "Los márgenes de los escritorios son áreas reservadas en el borde de su "
#~ "pantalla.  Las ventanas nuevas no serán colocadas dentro de un margen y "
#~ "las ventanas maximizas no las cubrirán."

#~ msgid "_Top"
#~ msgstr "_Arriba"

#~ msgid "_Left"
#~ msgstr "_Izquierda"

#~ msgid "_Right"
#~ msgstr "_Derecha"

#~ msgid "_Bottom"
#~ msgstr "A_bajo"

#~ msgid "Margins"
#~ msgstr "Márgenes"

#~ msgid "<span weight=\"bold\">Position</span>"
#~ msgstr "<span weight=\"bold\">Posición</span>"

#~ msgid "_Position:"
#~ msgstr "_Posición"

#~ msgid "Top Left"
#~ msgstr "Arriba a la derecha"

#~ msgid "Top"
#~ msgstr "Arriba"

#~ msgid "Top Right"
#~ msgstr "Arriba a la izquierda"

#~ msgid "Left"
#~ msgstr "Izquierda"

#~ msgid "Right"
#~ msgstr "Derecha"

#~ msgid "Bottom Left"
#~ msgstr "Abajo a la derecha"

#~ msgid "Bottom"
#~ msgstr "Abajo"

#~ msgid "Bottom Right"
#~ msgstr "Abajo a la derecha"

#~ msgid "Floating"
#~ msgstr "Flotando"

#~ msgid "_Floating position:"
#~ msgstr "Posición de _flotación:"

#~ msgid "x"
#~ msgstr "x"

#~ msgid "Allow _windows to be placed within the dock's area"
#~ msgstr ""
#~ "Permitir que las _ventanas sean colocadas dentro de el área del muelle"

#~ msgid "_Orientation: "
#~ msgstr "_Orientación: "

#~ msgid "Vertical"
#~ msgstr "Vertical"

#~ msgid "Horizontal"
#~ msgstr "Horizontal"

#~ msgid "<span weight=\"bold\">Stacking</span>"
#~ msgstr "<span weight=\"bold\">Apilando</span>"

#~ msgid "Keep dock _above other windows"
#~ msgstr "Mantener el muelle _sobre otras ventanas"

#~ msgid "A_llow dock to be both above and below windows"
#~ msgstr "Per_mitir que el muelle esté sobre y debajo de las ventanas"

#~ msgid "Keep dock _below other windows"
#~ msgstr "Mantener el muelle _bajo otras ventanas"

#~ msgid "<span weight=\"bold\">Hiding</span>"
#~ msgstr "<span weight=\"bold\">Ocultando</span>"

#~ msgid "_Hide off screen"
#~ msgstr "_Ocultar pantalla"

#~ msgid "_Delay before hiding:"
#~ msgstr "_Atrasar antes de ocultar:"

#~ msgid "Delay before _showing:"
#~ msgstr "Atra_sar antes de mostrar:"

#~ msgid "Dock"
#~ msgstr "Muelle"

#~ msgid "Abo_ut"
#~ msgstr "Acer_ca"

#~ msgid "About ObConf"
#~ msgstr "Acerca de ObConf"

#~ msgid "<span weight=\"bold\" size=\"xx-large\">ObConf VERSION</span>"
#~ msgstr "<span weight=\"bold\" size=\"xx-large\">ObConf VERSION</span>"

#~ msgid "A preferences manager for Openbox"
#~ msgstr "Un administrador de preferencias para Openbox"

#~ msgid "window1"
#~ msgstr "ventana1"

#~ msgid "<span weight=\"bold\">Press the key you wish to bind...</span>"
#~ msgstr "<span weight=\"bold\">Presione la letra que desea enlazar...</span>"

#~ msgid "--install requires an argument\n"
#~ msgstr "--install requiere un argumento\n"

#~ msgid "--archive requires an argument\n"
#~ msgstr "--archive requiere un argumento\n"

#~ msgid "--config-file requires an argument\n"
#~ msgstr "--archive requiere un argumento\n"

#~ msgid "(Unnamed desktop)"
#~ msgstr "(Escritorio sin nombre)"

#~ msgid "Custom actions"
#~ msgstr "Acciones personalizadas"
