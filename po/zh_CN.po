# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-11-13 23:54+0100\n"
"PO-Revision-Date: 2015-08-17 01:28+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Pootle 2.7\n"
"X-POOTLE-MTIME: 1439774923.450327\n"

#: ../src/main.c:177
msgid ""
"Failed to load the obconf.glade interface file. ObConf is probably not "
"installed correctly."
msgstr "加载 obconf.glade 接口文件失败。ObConf 可能没有正确安装。"

#: ../src/main.c:206
msgid "Failed to load an rc.xml. Openbox is probably not installed correctly."
msgstr "加载 rc.xml 失败。Openbox 可能没有正确安装。"

#: ../src/main.c:219
#, c-format
msgid ""
"Error while parsing the Openbox configuration file. Your configuration file "
"is not valid XML.\n"
"\n"
"Message: %s"
msgstr ""
"解析 Openbox 配置文件时出错。您的配置文件不是有效的 XML 文件。\n"
"\n"
"消息：%s"

#: ../src/theme.c:112 ../src/theme.c:144
msgid "Choose an Openbox theme"
msgstr "选择一个 Openbox 主题"

#: ../src/theme.c:121
msgid "Openbox theme archives"
msgstr "Openbox 主题档案"

#: ../src/archive.c:49
#, c-format
msgid "\"%s\" was installed to %s"
msgstr "“%s”已安装到 %s"

#: ../src/archive.c:74
#, c-format
msgid "\"%s\" was successfully created"
msgstr "“%s”成功创建"

#: ../src/archive.c:110
#, c-format
msgid ""
"Unable to create the theme archive \"%s\".\n"
"The following errors were reported:\n"
"%s"
msgstr ""
"无法创建主题档案“%s”。\n"
"报告了以下错误：\n"
"%s"

#: ../src/archive.c:115 ../src/archive.c:196
#, c-format
msgid "Unable to run the \"tar\" command: %s"
msgstr "无法运行“tar”命令：%s"

#: ../src/archive.c:135
#, c-format
msgid "Unable to create directory \"%s\": %s"
msgstr "无法创建“%s”目录：%s"

#: ../src/archive.c:157
#, c-format
msgid "\"%s\" does not appear to be a valid Openbox theme directory"
msgstr "“%s”似乎不是有效的 Openbox 主题目录"

#: ../src/archive.c:167
#, c-format
msgid "Unable to move to directory \"%s\": %s"
msgstr "无法移动到“%s”目录：%s"

#: ../src/archive.c:203
#, c-format
msgid ""
"Unable to extract the file \"%s\".\n"
"Please ensure that \"%s\" is writable and that the file is a valid Openbox "
"theme archive.\n"
"The following errors were reported:\n"
"%s"
msgstr ""
"无法提取“%s”文件。\n"
"请确信“%s”是可写的，且该文件是有效的 Openbox 主题档案。\n"
"报告了以下错误：\n"
"%s"

#: ../src/obconf.glade.h:1
msgid "<span weight=\"bold\">Theme</span>"
msgstr "<span weight=\"bold\">主题</span>"

#: ../src/obconf.glade.h:2
msgid "_Install a new theme..."
msgstr "安装新主题(_I)..."

#: ../src/obconf.glade.h:3
msgid "Create a theme _archive (.obt)..."
msgstr "创建一个主题档案(.obt)(_A)..."

#: ../src/obconf.glade.h:4
msgid "Theme"
msgstr "主题"

#: ../src/obconf.glade.h:5
msgid "Font for active window title:"
msgstr "活动窗口标题字体："

#: ../src/obconf.glade.h:6
msgid "Font for inactive window title:"
msgstr "非活动窗口标题字体："

#: ../src/obconf.glade.h:7
msgid "_Button order:"
msgstr "按钮顺序(_B)："

#: ../src/obconf.glade.h:8
msgid ""
"N: Window icon\n"
"L: Window label (Title)\n"
"I: Iconify (Minimize)\n"
"M: Maximize\n"
"C: Close\n"
"S: Shade (Roll up)\n"
"D: Omnipresent (On all desktops)"
msgstr ""
"N：窗口图标\n"
"L：窗口标签(标题)\n"
"I：图标化(最小化)\n"
"M：最大化\n"
"C：关闭\n"
"S：隐藏(卷起)\n"
"D: 总是出现(位于所有桌面)"

#: ../src/obconf.glade.h:15
msgid "Title Bar"
msgstr "标题栏"

#: ../src/obconf.glade.h:16
msgid "Font for menu header:"
msgstr "菜单标题字体："

#: ../src/obconf.glade.h:17
msgid "Font for menu Item:"
msgstr "菜单项字体："

#: ../src/obconf.glade.h:18
msgid "Font for on-screen display:"
msgstr "屏幕显示字体："

#: ../src/obconf.glade.h:19
msgid "Font for inactive on-screen display:"
msgstr "非活动的屏幕显示字体："

#: ../src/obconf.glade.h:20
msgid "Misc."
msgstr "杂项"
