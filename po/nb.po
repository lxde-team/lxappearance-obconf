# Norwegian translation for ObConf package.
# Copyright (C) 2007 Dana Jansens
# This file is distributed under the same license as the PACKAGE package.
# Michael Kjelbergvik Thung <postlogic@switch-case.org>, 2007.
#
msgid ""
msgstr ""
"Project-Id-Version: obconf 2.0.3\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-11-13 23:54+0100\n"
"PO-Revision-Date: 2015-08-17 01:28+0000\n"
"Last-Translator: Anonymous Pootle User\n"
"Language-Team: Norwegian\n"
"Language: nb\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Pootle 2.7\n"
"X-POOTLE-MTIME: 1439774920.762833\n"

#: ../src/main.c:177
msgid ""
"Failed to load the obconf.glade interface file. ObConf is probably not "
"installed correctly."
msgstr ""
"Klarte ikke lese inn grensesnittfila obconf.glade. ObConf er kanskje ikke "
"skikkelig installert."

#: ../src/main.c:206
msgid "Failed to load an rc.xml. Openbox is probably not installed correctly."
msgstr ""
"Klarte ikke lese inn noen rc.xml. Openbox er kanskje ikke ordentlig "
"installert."

#: ../src/main.c:219
#, c-format
msgid ""
"Error while parsing the Openbox configuration file. Your configuration file "
"is not valid XML.\n"
"\n"
"Message: %s"
msgstr ""
"Feil ved lesing av innstillingene til Openbox. Konfigurasjonsfila er ikke "
"gyldig XML.Melding: %s"

#: ../src/theme.c:112 ../src/theme.c:144
msgid "Choose an Openbox theme"
msgstr "Velg et Openbox-tema"

#: ../src/theme.c:121
msgid "Openbox theme archives"
msgstr "Openbox tema-arkiver"

#: ../src/archive.c:49
#, c-format
msgid "\"%s\" was installed to %s"
msgstr "\"%s\" ble installert i %s"

#: ../src/archive.c:74
#, c-format
msgid "\"%s\" was successfully created"
msgstr "\"%s\" ble opprettet"

#: ../src/archive.c:110
#, c-format
msgid ""
"Unable to create the theme archive \"%s\".\n"
"The following errors were reported:\n"
"%s"
msgstr ""
"Kan ikke lage tema-arkiv \"%s\".\n"
"Følgende feil ble rapportert:\n"
"%s"

#: ../src/archive.c:115 ../src/archive.c:196
#, c-format
msgid "Unable to run the \"tar\" command: %s"
msgstr "Kan ikke finne \"tar\"-kommandoen: %s"

#: ../src/archive.c:135
#, c-format
msgid "Unable to create directory \"%s\": %s"
msgstr "Kan ikke lage katalog \"%s\": %s"

#: ../src/archive.c:157
#, c-format
msgid "\"%s\" does not appear to be a valid Openbox theme directory"
msgstr "\"%s\" ser ikke ut til å være en gyldig katalog for Openbox-temaer"

#: ../src/archive.c:167
#, c-format
msgid "Unable to move to directory \"%s\": %s"
msgstr "Kan ikke flytte til katalog \"%s\": %s"

#: ../src/archive.c:203
#, c-format
msgid ""
"Unable to extract the file \"%s\".\n"
"Please ensure that \"%s\" is writable and that the file is a valid Openbox "
"theme archive.\n"
"The following errors were reported:\n"
"%s"
msgstr ""
"Kan ikke pakke ut filen \"%s\".\n"
"Vennligst pass på at \"%s\" kan skrives til og filen er et gyldig Openbox "
"tema-arkiv.\n"
"Følgende feil ble rapportert:\n"
"%s"

#: ../src/obconf.glade.h:1
msgid "<span weight=\"bold\">Theme</span>"
msgstr "<span weight=\"bold\">Tema</span>"

#: ../src/obconf.glade.h:2
msgid "_Install a new theme..."
msgstr "_Installér et nytt tema..."

#: ../src/obconf.glade.h:3
msgid "Create a theme _archive (.obt)..."
msgstr "Opprett et tema-_arkiv (.obt)..."

#: ../src/obconf.glade.h:4
msgid "Theme"
msgstr "Tema"

#: ../src/obconf.glade.h:5
msgid "Font for active window title:"
msgstr "Skrifttype i aktiv vindustittel: "

#: ../src/obconf.glade.h:6
msgid "Font for inactive window title:"
msgstr "Skrifttype i _inaktiv vindustittel: "

#: ../src/obconf.glade.h:7
msgid "_Button order:"
msgstr "_Knapp-rekkefølge"

#: ../src/obconf.glade.h:8
msgid ""
"N: Window icon\n"
"L: Window label (Title)\n"
"I: Iconify (Minimize)\n"
"M: Maximize\n"
"C: Close\n"
"S: Shade (Roll up)\n"
"D: Omnipresent (On all desktops)"
msgstr ""
"N:Vindusikon\n"
"L: Vindustittel\n"
"I: Minimer\n"
"M: Maksimer\n"
"C: Lukk\n"
"S: Rull opp\n"
"D: Vis på alle skrivebord"

#: ../src/obconf.glade.h:15
msgid "Title Bar"
msgstr "Vindustittel"

#: ../src/obconf.glade.h:16
msgid "Font for menu header:"
msgstr "Skrifttype i meny_overskriften: "

#: ../src/obconf.glade.h:17
msgid "Font for menu Item:"
msgstr "Skrifttype i menyen:"

#: ../src/obconf.glade.h:18
msgid "Font for on-screen display:"
msgstr "Skrifttype i skjermmeldinger: "

#: ../src/obconf.glade.h:19
#, fuzzy
msgid "Font for inactive on-screen display:"
msgstr "Skrifttype i skjermmeldinger: "

#: ../src/obconf.glade.h:20
msgid "Misc."
msgstr "Diverse"

#~ msgid "Syntax: obconf [options] [ARCHIVE.obt]\n"
#~ msgstr "Syntaks: obconf [alternativer] [ARKIV.obt]\n"

#~ msgid ""
#~ "\n"
#~ "Options:\n"
#~ msgstr ""
#~ "\n"
#~ "Alternativer:\n"

#~ msgid "  --help                Display this help and exit\n"
#~ msgstr "  --help                Vis denne teksten og avslutt\n"

#~ msgid "  --version             Display the version and exit\n"
#~ msgstr "  --version             Vis versjonsnummeret og avslutt\n"

#~ msgid ""
#~ "  --install ARCHIVE.obt Install the given theme archive and select it\n"
#~ msgstr "  --install ARKIV.obt   Installér valgt tema-arkiv og velg det\n"

#~ msgid ""
#~ "  --archive THEME       Create a theme archive from the given theme "
#~ "directory\n"
#~ msgstr ""
#~ "  --archive TEMA        Opprett et tema-arkiv fra oppgitt tema-katalog\n"

#~ msgid ""
#~ "\n"
#~ "Please report bugs at %s\n"
#~ "\n"
#~ msgstr ""
#~ "\n"
#~ "Vennligst rapporter feil til %s\n"
#~ "\n"

#~ msgid "Openbox Configuration Manager"
#~ msgstr "Openbox Konfigurasjonsbehandler"

#~ msgid "    "
#~ msgstr "    "

#~ msgid "<span weight=\"bold\">Windows</span>"
#~ msgstr "<span weight=\"bold\">Vinduer</span>"

#~ msgid "_Windows retain a border when undecorated"
#~ msgstr "_Vinduer beholder en ramme når de er udekorerte"

#~ msgid "A_nimate iconify and restore"
#~ msgstr "A_nimer minimering og gjenoppretting"

#~ msgid "<span weight=\"bold\">Window Titles</span>"
#~ msgstr "<span weight=\"bold\">Vindustitler</span>"

#~ msgid ""
#~ "N - The window's icon\n"
#~ "D - The all-desktops (sticky) button\n"
#~ "S - The shade (roll up) button\n"
#~ "L - The label (window title)\n"
#~ "I - The iconify (minimize) button\n"
#~ "M - The maximize button\n"
#~ "C - The close button"
#~ msgstr ""
#~ "N - Vinduets ikon\n"
#~ "D - Knapp for alle skrivebord (sticky)\n"
#~ "S - Knapp for å rulle opp vinduet\n"
#~ "L - Vindustittel\n"
#~ "I - Knapp for å minimisere vinduet\n"
#~ "M - Knapp for å maksimere vinduet\n"
#~ "C - Knapp for å lukke vinduet"

#~ msgid "<span weight=\"bold\">Fonts</span>"
#~ msgstr "<span weight=\"bold\">Skrifttyper</span>"

#~ msgid "_Active window title: "
#~ msgstr "_Aktiv vindustittel: "

#~ msgid "_Menu Item: "
#~ msgstr "_Menyalternativ: "

#~ msgid "Appearance"
#~ msgstr "Utseende"

#~ msgid "<span weight=\"bold\">Focusing Windows</span>"
#~ msgstr "<span weight=\"bold\">Vindusfokus</span>"

#~ msgid "Focus _new windows when they appear"
#~ msgstr "Fokuser på _nye vinduer når de vises"

#~ msgid "<span weight=\"bold\">Placing Windows</span>"
#~ msgstr "<span weight=\"bold\">Vindusplassering</span>"

#~ msgid "_Place new windows under the mouse pointer"
#~ msgstr "_Plassér nye vinduer under musepekeren"

#~ msgid "_Center new windows when they are placed"
#~ msgstr "_Sentrér nye vinduer"

#~ msgid "Windows"
#~ msgstr "Vinduer"

#~ msgid "<span weight=\"bold\">Moving and Resizing Windows</span>"
#~ msgstr ""
#~ "<span weight=\"bold\">Flytting og Størrelsesendring av Vinduer</span>"

#~ msgid "Update the window contents while _resizing"
#~ msgstr "Oppdater vinduets innhold under _størrelsesendring"

#~ msgid "Drag _threshold distance:"
#~ msgstr "Dra-_terskel"

#~ msgid "px"
#~ msgstr "px"

#~ msgid "Amount of resistance against other _windows:"
#~ msgstr "Motstand mot andre _vinduer:"

#~ msgid "Amount of resistance against screen _edges:"
#~ msgstr "Motstand mot skjerm_kanter:"

#~ msgid "_Switch desktops when moving a window past the screen edge"
#~ msgstr "_Bytt skrivebord når et vindu flyttes forbi skjermkanten"

#~ msgid "_Amount of time to wait before switching:"
#~ msgstr "_Forsinkelse før bytte:"

#~ msgid "ms"
#~ msgstr "ms"

#~ msgid "When resizing terminal windows"
#~ msgstr "For terminalvinduer"

#~ msgid "Always"
#~ msgstr "Alltid"

#~ msgid "Never"
#~ msgstr "Aldri"

#~ msgid "Centered on the window"
#~ msgstr "Sentrert på vinduet"

#~ msgid "Above the window"
#~ msgstr "Over vinduet"

#~ msgid "_Focus windows when the mouse pointer moves over them"
#~ msgstr "_Fokusér når musepekeren beveges over vinduer"

#~ msgid "Move focus under the mouse when _switching desktops"
#~ msgstr "Flytt fokus under musepekeren ved _bytting av skrivebord"

#~ msgid "Move focus _under the mouse when the mouse is not moving"
#~ msgstr "Flytt fokus _under musepekeren når musen ikke flyttes"

#~ msgid "_Raise windows when the mouse pointer moves over them"
#~ msgstr "_Hev vinduer når musepekeren flyttes over dem"

#~ msgid "_Delay before focusing and raising windows:"
#~ msgstr "Forsinkelse før fokusering og heving av vinduer:"

#~ msgid "<span weight=\"bold\">Titlebar</span>"
#~ msgstr "<span weight=\"bold\">Tittellinje</span>"

#~ msgid "Double click on the _titlebar:"
#~ msgstr "Dobbelklikk på tittellinjen:"

#~ msgid "Maximizes the window"
#~ msgstr "Maksimerer vinduet"

#~ msgid "Shades the window"
#~ msgstr "Ruller opp vinduet"

#~ msgid "Double click ti_me:"
#~ msgstr "Dobbelklikkstid"

#~ msgid "Mouse"
#~ msgstr "Mus"

#~ msgid "<span weight=\"bold\">Desktops</span>"
#~ msgstr "<span weight=\"bold\">Skrivebord</span>"

#~ msgid "_Show a notification when switching desktops"
#~ msgstr "_Vis en melding ved skrivebordsbytte"

#~ msgid "_Amount of time to show the notification for:"
#~ msgstr "Visningstid for _meldingen:"

#~ msgid "_Number of desktops: "
#~ msgstr "_Antall skrivebord: "

#~ msgid "_Desktop names:"
#~ msgstr "_Skrivebordsnavn"

#~ msgid "Desktops"
#~ msgstr "Skrivebord"

#~ msgid "<span weight=\"bold\">Desktop Margins</span>"
#~ msgstr "<span weight=\"bold\">Skrivebordsmarginer</span>"

#~ msgid ""
#~ "Desktop margins are reserved areas on the edge of your screen.  New "
#~ "windows will not be placed within a margin, and maximized windows will "
#~ "not cover them."
#~ msgstr ""
#~ "Skrivebordsmarginer er reserverte områder ved kantene på skjermen. Nye "
#~ "vinduer vil ikke bli plassert over en margin, og maksimerte vinduer vil "
#~ "ikke overlappe dem."

#~ msgid "_Top"
#~ msgstr "_Topp"

#~ msgid "_Left"
#~ msgstr "_Venstre"

#~ msgid "_Right"
#~ msgstr "_Høyre"

#~ msgid "_Bottom"
#~ msgstr "_Bunn"

#~ msgid "Margins"
#~ msgstr "_Marginer"

#~ msgid "<span weight=\"bold\">Position</span>"
#~ msgstr "<span weight=\"bold\">Posisjon</span>"

#~ msgid "_Position:"
#~ msgstr "_Posisjon:"

#~ msgid "Top Left"
#~ msgstr "Øverst til venstre"

#~ msgid "Top"
#~ msgstr "Topp"

#~ msgid "Top Right"
#~ msgstr "Øverst til høyre"

#~ msgid "Left"
#~ msgstr "Venstre"

#~ msgid "Right"
#~ msgstr "Høyre"

#~ msgid "Bottom Left"
#~ msgstr "Nederst til venstre"

#~ msgid "Bottom"
#~ msgstr "Bunn"

#~ msgid "Bottom Right"
#~ msgstr "Nederst til høyre"

#~ msgid "Floating"
#~ msgstr "Svevende"

#~ msgid "_Floating position:"
#~ msgstr "_Svevende posisjon:"

#~ msgid "x"
#~ msgstr "x"

#~ msgid "Allow _windows to be placed within the dock's area"
#~ msgstr "Tillat _vinduer å bli plassert i dockens område"

#~ msgid "_Orientation: "
#~ msgstr "_Orientering: "

#~ msgid "Vertical"
#~ msgstr "Vertikal"

#~ msgid "Horizontal"
#~ msgstr "Horisontal"

#~ msgid "<span weight=\"bold\">Stacking</span>"
#~ msgstr "<span weight=\"bold\">Stabling</span>"

#~ msgid "Keep dock _above other windows"
#~ msgstr "Hold docken _over_ andre vinduer"

#~ msgid "A_llow dock to be both above and below windows"
#~ msgstr "Ti_llat docken å være både over og under vinduer"

#~ msgid "Keep dock _below other windows"
#~ msgstr "Hold docken _under andre vinduer"

#~ msgid "<span weight=\"bold\">Hiding</span>"
#~ msgstr "<span weight=\"bold\">Skjuling</span>"

#~ msgid "_Hide off screen"
#~ msgstr "_Skjul utenfor skjermen"

#~ msgid "_Delay before hiding:"
#~ msgstr "_Forsinkelse før skjuling:"

#~ msgid "Delay before _showing:"
#~ msgstr "Forsinkelse før vi_sning:"

#~ msgid "Dock"
#~ msgstr "Dock"

#~ msgid "Abo_ut"
#~ msgstr "_Om"

#~ msgid "About ObConf"
#~ msgstr "Om ObConf"

#~ msgid "A preferences manager for Openbox"
#~ msgstr "En konfigurasjonsbehandler for Openbox"

#~ msgid "<span weight=\"bold\">Press the key you wish to bind...</span>"
#~ msgstr "<span weight=\"bold\">Trykk på knappen du ønsker å bruke...</span>"

#~ msgid "--install requires an argument\n"
#~ msgstr "--install krever et argument\n"

#~ msgid "--archive requires an argument\n"
#~ msgstr "--archive krever et argument\n"

#, fuzzy
#~ msgid "--config-file requires an argument\n"
#~ msgstr "--archive krever et argument\n"

#~ msgid "(Unnamed desktop)"
#~ msgstr "(Navnløst skrivebord)"

#~ msgid "Custom actions"
#~ msgstr "Egendefinerte handlinger"
